/**
 * Created by zzy on 2019/7/4.
 */
const apiModule = {
  // 查询
  query: {
    url: '/app/h5/loadProject',
    method: 'get'
  },
  // 表单提交
  submit: {
    url: '/app/h5/submitForm',
    method: 'post'
  },
  // 微信分享
  shareData: {
    url: '/app/share/getShareData',
    method: 'post'
  }
}

export default apiModule
