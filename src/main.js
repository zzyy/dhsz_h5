// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ajax from '@/utils/ajax'
import api from './api'
import VueWechatTitle from 'vue-wechat-title'
import {
  WechatPlugin,
  XInput,
  XTextarea,
  Rater,
  XSwitch,
  PopupPicker,
  Group,
  Datetime,
  PopupRadio,
  Checklist,
  Cell,
  XButton,
  Box,
  ToastPlugin
} from 'vux'
import VueVideoPlayer from 'vue-video-player'
import 'video.js/dist/video-js.css'

// import VConsole from 'vconsole'
// let vConsole = new VConsole()
// console.log(vConsole)

Vue.use(VueVideoPlayer)
// 使用微信 jssdk
Vue.use(WechatPlugin)
Vue.component('x-input', XInput)
Vue.component('x-textarea', XTextarea)
Vue.component('rater', Rater)
Vue.component('x-switch', XSwitch)
Vue.component('popup-picker', PopupPicker)
Vue.component('group', Group)
Vue.component('datetime', Datetime)
Vue.component('popup-radio', PopupRadio)
Vue.component('checklist', Checklist)
Vue.component('cell', Cell)
Vue.component('x-button', XButton)
Vue.component('box', Box)
Vue.use(ToastPlugin)

// 移除移动端页面点击延迟
const FastClick = require('../static/fastclick')
FastClick.attach(document.body)

Vue.prototype.$ajax = ajax
Vue.prototype.$api = api

Vue.config.productionTip = false

Vue.use(VueWechatTitle)
// 统计代码
/* eslint-disable */
router.beforeEach((to, from, next) => {
  if (to.meta.title) {
    document.title = to.meta.title
  }
  setTimeout(() => {
    let _mtac = {}
    let mta = document.createElement('script')
    mta.src = '//pingjs.qq.com/h5/stats.js?v2.0.4'
    mta.setAttribute('name', 'MTAH5')
    mta.setAttribute('sid', '500712118')
    let s = document.getElementsByTagName('script')[0]
    s.parentNode.insertBefore(mta, s)
  }, 50)
  next()
})
/* eslint-enable */

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
