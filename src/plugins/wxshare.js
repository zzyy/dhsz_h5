import ajax from '@/utils/ajax'
import api from '@/api'
import Vue from 'vue'
import { WechatPlugin } from 'vux'
// 使用微信 jssdk
Vue.use(WechatPlugin)

const wxshare = function (msg) {
  // 得到浏览器环境
  const wx = (() => {
    return navigator.userAgent.toLowerCase().indexOf('micromessenger') !== -1
  })()
  if (wx) {
    let url = ''
    if (window.__wxjs_is_wkwebview === true) {
      // 如果当前设备是IOS
      url = window.location.href.split('#')[0]
    } else {
      // 非IOS设备
      url = window.location.href
    }
    ajax(api.shareData, { url: encodeURIComponent(url) }).then(
      res => {
        if (res.code === 0) {
          let wxData = res.data
          if (wxData.signature !== null) {
            Vue.wechat.config({
              debug: false,
              appId: wxData.appId,
              timestamp: wxData.timestamp,
              nonceStr: wxData.nonceStr,
              signature: wxData.signature,
              jsApiList: ['checkJsApi', 'onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareQZone']
            })

            let shareConfig = {
              imgUrl: msg.imgUrl,
              desc: msg.desc,
              title: msg.title,
              link: msg.link,
              success: () => {},
              cancel: () => {}
            }

            Vue.wechat.ready(() => {
              Vue.wechat.onMenuShareTimeline(shareConfig)
              Vue.wechat.onMenuShareAppMessage(shareConfig)
              Vue.wechat.onMenuShareQQ(shareConfig)
              Vue.wechat.onMenuShareQZone(shareConfig)

              Vue.wechat.error((res) => {
                console.log(res.errMsg)
              })
            })
          }
        } else {
          console.log(res.data.msg)
        }
      }
    )
  }
}

export default wxshare
