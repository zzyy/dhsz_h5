import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/home'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/:projectId/:type',
      name: 'Home',
      component: Home
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.params.type) {
    next()
  } else {
    let p = to.fullPath
    p.substr(p.length - 1, 1) === '/'
      ? next(to.fullPath + '1/')
      : next(to.fullPath + '/1/')
  }
})

export default router
