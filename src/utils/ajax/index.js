import Vue from 'vue'
import {ToastPlugin} from 'vux'
import axios from 'axios'

Vue.use(ToastPlugin)

const ROOT = process.env.API_URL

// 请求时
axios.interceptors.request.use(
  config => {
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// 请求完成
axios.interceptors.response.use(
  response => {
    return response
  },
  error => {
    return Promise.reject(error)
  }
)

function successState (res) {
  // 判断后台返回错误码
  if (res.data.code === 500 || res.data.code === 601) {
    Vue.$vux.toast.show({
      text: res.data.msg,
      type: 'warn',
      width: '12em'
    })
  }
  if (res.data.code === 700) {
    Vue.$vux.toast.show({
      text: res.data.msg,
      type: 'warn',
      width: '12em'
    })
  }
}

function errorState (err) {
  // 状态码为200，直接返回数据
  if (err && (err.status === 200 || err.status === 304 || err.status === 400)) {
    return err
  } else if (err.status === 504) {
    Vue.$vux.toast.show({
      text: err.statusText,
      type: 'warn',
      width: '12em'
    })
  } else if (err.status === 500) {
    Vue.$vux.toast.show({
      text: '系统异常，请联系管理员',
      type: 'warn',
      width: '12em'
    })
  } else {
    Vue.$vux.toast.show({
      text: err.data.message,
      type: 'warn',
      width: '12em'
    })
  }
}

const httpServer = (opts, data) => {
  let httpDefaultOpts = {
    method: opts.method,
    url: opts.url,
    timeout: 60000,
    baseURL: ROOT,
    // get
    params: {},
    data: data,
    headers: opts.method === 'get' ? {
      'X-Requested-With': 'XMLHttpRequest',
      'Accept': 'application/json',
      'Content-Type': 'application/json;charset=UTF-8'
    } : {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/json;charset=UTF-8'
    }
  }

  if (opts.method === 'get') {
    delete httpDefaultOpts.data
  } else {
    delete httpDefaultOpts.params
  }

  return new Promise((resolve, reject) => {
    axios(httpDefaultOpts)
      .then(res => {
        successState(res)
        resolve(res.data)
      })
      .catch(err => {
        errorState(err)
        reject(err)
      })
  })
}

export default httpServer
